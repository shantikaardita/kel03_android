import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

Mobile.startApplication('/home/edi/apkbinar/app-release-second-hand-gcp.apk', false)

Mobile.tap(findTestObject('TC018_Seller Contact Buyer_Positive/android.widget.FrameLayout'), 60)

Mobile.tap(findTestObject('TC018_Seller Contact Buyer_Positive/android.widget.Button - Masuk'), 60)

Mobile.doubleTap(findTestObject('TC018_Seller Contact Buyer_Positive/android.widget.EditText - Masukkan email'), 60, FailureHandling.STOP_ON_FAILURE)

Mobile.waitForElementPresent(findTestObject('TC018_Seller Contact Buyer_Positive/android.widget.EditText - Masukkan email'), 
    60)

Mobile.setText(findTestObject('TC018_Seller Contact Buyer_Positive/android.widget.EditText - Masukkan email'), 'studentqa2@binar.id', 
    60)

Mobile.doubleTap(findTestObject('TC018_Seller Contact Buyer_Positive/android.widget.EditText - Masukkan password'), 60)

Mobile.waitForElementPresent(findTestObject('TC018_Seller Contact Buyer_Positive/android.widget.EditText - Masukkan password'), 
    60)

Mobile.setText(findTestObject('TC018_Seller Contact Buyer_Positive/android.widget.EditText - Masukkan password'), 'studentQA2', 
    60)

Mobile.tap(findTestObject('TC018_Seller Contact Buyer_Positive/android.widget.Button - Masuk (1)'), 60)

Mobile.tap(findTestObject('TC018_Seller Contact Buyer_Positive/android.widget.TextView - Daftar Jual Saya'), 60)

Mobile.tap(findTestObject('TC018_Seller Contact Buyer_Positive/android.widget.TextView - Diminati'), 60)

Mobile.waitForElementPresent(findTestObject('TC018_Seller Contact Buyer_Positive/android.view.ViewGroup'), 60)

Mobile.tap(findTestObject('TC018_Seller Contact Buyer_Positive/android.view.ViewGroup'), 60)

Mobile.tap(findTestObject('TC018_Seller Contact Buyer_Positive/android.widget.Button - Terima'), 60)

Mobile.tap(findTestObject('TC018_Seller Contact Buyer_Positive/android.widget.Button - Hubungi via Whatsapp'), 60)


import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

Mobile.startApplication('C:\\Users\\app-release-second-hand-gcp.apk', true)

Mobile.tap(findTestObject('Object Repository/05 Offer Processing - Camera/TC015 Seller Add Product - Cam/android.widget.TextView - Akun'), 0)

Mobile.tap(findTestObject('Object Repository/05 Offer Processing - Camera/TC015 Seller Add Product - Cam/android.widget.Button - Masuk'), 0)

Mobile.setText(findTestObject('Object Repository/05 Offer Processing - Camera/TC015 Seller Add Product - Cam/android.widget.EditText - Masukkan email'), 
    'kel03fnseller@gmail.com', 0)

Mobile.setEncryptedText(findTestObject('Object Repository/05 Offer Processing - Camera/TC015 Seller Add Product - Cam/android.widget.EditText - Masukkan password'), 
    'aeHFOx8jV/A=', 0)

Mobile.tap(findTestObject('Object Repository/05 Offer Processing - Camera/TC015 Seller Add Product - Cam/android.widget.Button - Masuk (1)'), 0)

Mobile.tap(findTestObject('Object Repository/05 Offer Processing - Camera/TC015 Seller Add Product - Cam/android.widget.ImageButton'), 0)

Mobile.setText(findTestObject('Object Repository/05 Offer Processing - Camera/TC015 Seller Add Product - Cam/android.widget.EditText - Nama Produk'), 'FZ12321', 
    0)

Mobile.setText(findTestObject('Object Repository/05 Offer Processing - Camera/TC015 Seller Add Product - Cam/android.widget.EditText - Rp 0,00'), '100000', 
    0)

Mobile.tap(findTestObject('Object Repository/05 Offer Processing - Camera/TC015 Seller Add Product - Cam/android.widget.Spinner - Pilih Kategori'), 0)

Mobile.tap(findTestObject('Object Repository/05 Offer Processing - Camera/TC015 Seller Add Product - Cam/android.widget.EditText - Lokasi Produk'), 0)

Mobile.setText(findTestObject('Object Repository/05 Offer Processing - Camera/TC015 Seller Add Product - Cam/android.widget.EditText - Lokasi Produk (1)'), 
    'Jakarta', 0)

Mobile.setText(findTestObject('Object Repository/05 Offer Processing - Camera/TC015 Seller Add Product - Cam/android.widget.EditText - Contoh Barang bagus mantap'), 
    'Barang bagus', 0)

Mobile.tap(findTestObject(''), 0)

Mobile.tap(findTestObject(''), 0)

Mobile.tap(findTestObject(''), 0)

Mobile.tap(findTestObject(''), 0)

Mobile.tap(findTestObject('Object Repository/05 Offer Processing - Camera/TC015 Seller Add Product - Cam/android.widget.Button - Terbitkan'), 0)

Mobile.pressBack()

Mobile.tap(findTestObject('Object Repository/05 Offer Processing - Camera/TC015 Seller Add Product - Cam/android.widget.TextView - Keluar'), 0)

Mobile.closeApplication()


import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

Mobile.startApplication('C:\\Users\\app-release-second-hand-gcp.apk', false)

Mobile.waitForElementPresent(findTestObject('05 Offer Processing/1501 Registrasi Akun/Icon_Akun'), 10)

Mobile.tap(findTestObject('05 Offer Processing/1501 Registrasi Akun/Icon_Akun'), 0)

Mobile.tap(findTestObject('05 Offer Processing/1501 Registrasi Akun/Button_Masuk'), 0)

Mobile.tap(findTestObject('05 Offer Processing/1501 Registrasi Akun/Button_Daftar'), 0)

Mobile.setText(findTestObject('05 Offer Processing/1501 Registrasi Akun/Daftar_InputNama'), 'Seller - Kelompok 3 Platinum', 
    0)

//Generate email
Date email = new Date()

String emailDoctor = email.format('yyyyMMddHHmmss')

def email_code = ('seller_kel03_and' + emailDoctor) + '@gmail.com'

//Set GlobalVariable
GlobalVariable.emailVarSel = email_code

//Set GlobalVariable for datetime
GlobalVariable.datetimeVar = emailDoctor

Mobile.setText(findTestObject('05 Offer Processing/1501 Registrasi Akun/Daftar_InputEmail'), GlobalVariable.emailVarSel, 
    0)

Mobile.setEncryptedText(findTestObject('05 Offer Processing/1501 Registrasi Akun/Daftar_InputPassword'), 'aeHFOx8jV/A=', 
    0)

Mobile.setText(findTestObject('05 Offer Processing/1501 Registrasi Akun/Daftar_InputNoHP'), '081231231231', 0)

Mobile.setText(findTestObject('05 Offer Processing/1501 Registrasi Akun/Daftar_InputKota'), 'Jakarta', 0)

Mobile.scrollToText('', FailureHandling.STOP_ON_FAILURE)

Mobile.setText(findTestObject('05 Offer Processing/1501 Registrasi Akun/Daftar_InputAlamat'), 'Jalan Mawar ' + GlobalVariable.datetimeVar, 
    0)

Mobile.scrollToText('', FailureHandling.STOP_ON_FAILURE)

Mobile.tap(findTestObject('05 Offer Processing/1501 Registrasi Akun/Registrasi_ButtonXpath'), 0)

Mobile.waitForElementPresent(findTestObject('05 Offer Processing/1502 Add Product/Button - Plus'), 10)

Mobile.tap(findTestObject('05 Offer Processing/1502 Add Product/Button - Plus'), 0)

def product_code = 'MtFuji' + GlobalVariable.datetimeVar

//Set GlobalVariable productIDname
GlobalVariable.productIDVar = product_code

Mobile.setText(findTestObject('05 Offer Processing/1502 Add Product/Text - Nama Produk'), GlobalVariable.productIDVar, 0)

Mobile.setText(findTestObject('05 Offer Processing/1502 Add Product/Text - Harga Produk'), '100000', 0)

Mobile.tap(findTestObject('05 Offer Processing/1502 Add Product/Spinner - Pilih Kategori'), 0)

Mobile.delay(2, FailureHandling.STOP_ON_FAILURE)

Mobile.tap(findTestObject('05 Offer Processing/1502 Add Product/Text - Lokasi Produk'), 0, FailureHandling.STOP_ON_FAILURE)

Mobile.setText(findTestObject('05 Offer Processing/1502 Add Product/Text - Lokasi Produk'), 'Jakarta', 0)

Mobile.setText(findTestObject('05 Offer Processing/1502 Add Product/Text - Deskripsi'), 'Mount Fuji Summer 2023', 0)

Mobile.delay(2, FailureHandling.STOP_ON_FAILURE)

Mobile.tap(findTestObject('05 Offer Processing/1502 Add Product/Cam01 - btn - Foto Product'), 0)

Mobile.waitForElementPresent(findTestObject('05 Offer Processing/1502 Add Product/Cam02 - Button - Kamera'), 10)

Mobile.tap(findTestObject('05 Offer Processing/1502 Add Product/Cam02 - Button - Kamera'), 0)

Mobile.waitForElementPresent(findTestObject('05 Offer Processing/1502 Add Product/Cam03 - ImageView - Shoot'), 10)

Mobile.tap(findTestObject('05 Offer Processing/1502 Add Product/Cam03 - ImageView - Shoot'), 0)

Mobile.waitForElementPresent(findTestObject('05 Offer Processing/1502 Add Product/Cam04 - ImageButton - Check OK'), 10)

Mobile.tap(findTestObject('05 Offer Processing/1502 Add Product/Cam04 - ImageButton - Check OK'), 0)

Mobile.waitForElementPresent(findTestObject('05 Offer Processing/1502 Add Product/Button - Terbitkan'), 10)

Mobile.tap(findTestObject('05 Offer Processing/1502 Add Product/Button - Terbitkan'), 0)

Mobile.waitForElementPresent(findTestObject('05 Offer Processing/1502 Add Product/99-01 ViewGroup'), 10)

Mobile.verifyElementVisible(findTestObject('05 Offer Processing/1502 Add Product/99-02 ImageView - trash'), 0)

Mobile.delay(2, FailureHandling.STOP_ON_FAILURE)

Mobile.tap(findTestObject('05 Offer Processing/1502 Add Product/99-03 ImageView - back'), 0)

Mobile.tap(findTestObject('05 Offer Processing/1502 Add Product/99-04 TextView - Keluar'), 0)

Mobile.delay(3, FailureHandling.STOP_ON_FAILURE)


import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

Mobile.startApplication('C:\\Users\\app-release-second-hand-gcp.apk', false)

Mobile.tap(findTestObject('05 Offer Processing/TC014 Buy Product Negative/icon Akun'), 0)

Mobile.delay(2, FailureHandling.STOP_ON_FAILURE)

Mobile.doubleTap(findTestObject('05 Offer Processing/TC014 Buy Product Negative/button_login'), 0)

Mobile.setText(findTestObject('05 Offer Processing/TC014 Buy Product Negative/Input Email'), 'kel03_and@gmail.com', 0)

Mobile.setEncryptedText(findTestObject('05 Offer Processing/TC014 Buy Product Negative/Input Password'), 'aeHFOx8jV/A=', 
    0)

Mobile.tap(findTestObject('05 Offer Processing/TC014 Buy Product Negative/Button Login1'), 0)

Mobile.tap(findTestObject('05 Offer Processing/TC014 Buy Product Negative/Icon_Beranda'), 0)

Mobile.delay(20, FailureHandling.STOP_ON_FAILURE)

Mobile.tap(findTestObject('05 Offer Processing/TC014 Buy Product Negative/Beli_Produk_Teratas'), 0, FailureHandling.STOP_ON_FAILURE)

Mobile.delay(3, FailureHandling.STOP_ON_FAILURE)

Mobile.tap(findTestObject('05 Offer Processing/TC013 Buy Product Positive/Tombol_Nego'), 0)

Mobile.delay(2, FailureHandling.STOP_ON_FAILURE)

Mobile.verifyElementText(findTestObject('05 Offer Processing/TC014 Buy Product Negative/Field_Harga_Tawar'), '')

Mobile.tap(findTestObject('05 Offer Processing/TC014 Buy Product Negative/android.widget.EditText - Rp 0,00'), 0)

Mobile.setText(findTestObject('05 Offer Processing/TC014 Buy Product Negative/android.widget.EditText - Rp 0,00'), '', 0)

Mobile.pressBack()

Mobile.tap(findTestObject('05 Offer Processing/TC014 Buy Product Negative/android.widget.Button - Kirim'), 0)

Mobile.delay(2, FailureHandling.STOP_ON_FAILURE)


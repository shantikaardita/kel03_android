import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

Mobile.startApplication('C:\\Users\\app-release-second-hand-gcp.apk', false)

Mobile.waitForElementPresent(findTestObject('05 Offer Processing/1602 Offer Product/01-01 ViewGroup - Beranda'), 10 // adjust the timeout value as needed
    )

Mobile.tap(findTestObject('05 Offer Processing/1602 Offer Product/01-01 ViewGroup - Beranda'), 0, FailureHandling.CONTINUE_ON_FAILURE)

Mobile.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

// Step 1: Tap Search button
Mobile.tap(findTestObject('05 Offer Processing/1602 Offer Product/02-02Tap EditText - Cari di Second Chance'), 0, FailureHandling.STOP_ON_FAILURE)

Mobile.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

// Step 2: Type the search 'MtFuji'
Mobile.setText(findTestObject('05 Offer Processing/1602 Offer Product/02-03Type EditText - Cari di Second Chance'), 'MtFuji', 
    0)

// Step 3: Wait for the results to show up
Mobile.waitForElementPresent(findTestObject('05 Offer Processing/1602 Offer Product/03 ImageView'), 10 // adjust the timeout value as needed
    )

Mobile.tap(findTestObject('05 Offer Processing/1602 Offer Product/03 ImageView'), 0)

Mobile.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

Mobile.tap(findTestObject('05 Offer Processing/1602 Offer Product/04 Button - Saya Tertarik dan Ingin Nego'), 0)

Mobile.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

Mobile.setText(findTestObject('05 Offer Processing/1602 Offer Product/05 EditText - Rp 0,00 Harga Tawar'), '9000', 0)

Mobile.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

Mobile.tap(findTestObject('05 Offer Processing/1602 Offer Product/06 Button - Kirim'), 0)


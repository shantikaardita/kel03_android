import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

Mobile.startApplication('C:\\Users\\app-release-second-hand-gcp.apk', false)

Mobile.tap(findTestObject('05 Offer Processing/1502 Add Product/Button - Plus'), 0)

def product_code = ('MtFuji' + GlobalVariable.datetimeVar)

//Set GlobalVariable productIDname
GlobalVariable.productIDVar = product_code

Mobile.setText(findTestObject('05 Offer Processing/1502 Add Product/Text - Nama Produk'), GlobalVariable.productIDVar, 0)

Mobile.setText(findTestObject('05 Offer Processing/1502 Add Product/Text - Harga Produk'), '10000', 0)

Mobile.tap(findTestObject('05 Offer Processing/1502 Add Product/Spinner - Pilih Kategori'), 0)

Mobile.delay(2, FailureHandling.STOP_ON_FAILURE)

Mobile.tap(findTestObject('05 Offer Processing/1502 Add Product/Text - Lokasi Produk'), 0, FailureHandling.STOP_ON_FAILURE)

Mobile.setText(findTestObject('05 Offer Processing/1502 Add Product/Text - Lokasi Produk'), 'Jakarta', 0)

Mobile.setText(findTestObject('05 Offer Processing/1502 Add Product/Text - Deskripsi'), 'Mount Fuji Summer 2023', 0)

Mobile.delay(2, FailureHandling.OPTIONAL)

Mobile.tap(findTestObject('05 Offer Processing/1502 Add Product/android.widget.ImageView'), 0)

Mobile.delay(2, FailureHandling.OPTIONAL)

Mobile.tap(findTestObject('05 Offer Processing/1502 Add Product/android.widget.Button - Galeri'), 0)

Mobile.delay(2, FailureHandling.OPTIONAL)

Mobile.tap(findTestObject('05 Offer Processing/1502 Add Product/ImageView - triple dot'), 0)

Mobile.delay(2, FailureHandling.OPTIONAL)

Mobile.tap(findTestObject('05 Offer Processing/1502 Add Product/TextView - Browse'), 0)

Mobile.delay(2, FailureHandling.OPTIONAL)

Mobile.tap(findTestObject('05 Offer Processing/1502 Add Product/GDrive01 - triple bar'), 0)

Mobile.delay(2, FailureHandling.OPTIONAL)

Mobile.tap(findTestObject('05 Offer Processing/1502 Add Product/GDrive02 - Drive'), 0)

Mobile.delay(2, FailureHandling.OPTIONAL)

Mobile.tap(findTestObject('05 Offer Processing/1502 Add Product/GDrive03 - My Drive'), 0)

Mobile.delay(2, FailureHandling.OPTIONAL)

Mobile.tap(findTestObject('05 Offer Processing/1502 Add Product/GDrive04 - Images'), 0)

Mobile.delay(2, FailureHandling.OPTIONAL)

Mobile.tap(findTestObject('05 Offer Processing/1502 Add Product/GDrive05 - 05_Offer'), 0)

Mobile.delay(2, FailureHandling.OPTIONAL)

Mobile.tap(findTestObject('05 Offer Processing/1502 Add Product/GDrive06 - ImageView'), 0)

Mobile.delay(2, FailureHandling.OPTIONAL)

Mobile.tap(findTestObject('05 Offer Processing/1502 Add Product/Button - Terbitkan'), 0)


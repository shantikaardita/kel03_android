import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

Mobile.tap(findTestObject('06 AddNewProduct/android.widget.ImageButton'), 0)

Mobile.tap(findTestObject('06 AddNewProduct/android.widget.EditText - Nama Produk'), 0)

Mobile.setText(findTestObject('06 AddNewProduct/android.widget.EditText - Nama Produk (1)'), 'produk wididay mobile', 0)

Mobile.hideKeyboard()

Mobile.tap(findTestObject('06 AddNewProduct/android.widget.EditText - Rp 0,00'), 0)

Mobile.setText(findTestObject('06 AddNewProduct/android.widget.EditText - Rp 0,00 (1)'), '20000', 0)

Mobile.hideKeyboard()

Mobile.tap(findTestObject('06 AddNewProduct/android.widget.Spinner - Pilih Kategori'), 0)

Mobile.tap(findTestObject('06 AddNewProduct/android.widget.EditText - Lokasi Produk'), 0)

Mobile.tap(findTestObject('06 AddNewProduct/android.widget.EditText - Lokasi Produk (1)'), 0)

Mobile.setText(findTestObject('06 AddNewProduct/android.widget.EditText - Lokasi Produk (2)'), 'depok', 0)

Mobile.hideKeyboard()

Mobile.tap(findTestObject('Object Repository/06 AddNewProduct/android.widget.EditText - Contoh Barang bagus mantap'), 0)

Mobile.setText(findTestObject('Object Repository/06 AddNewProduct/android.widget.EditText - Contoh Barang bagus mantap (1)'), 
    'bagus bagus ajasih', 0)

Mobile.hideKeyboard()

Mobile.tap(findTestObject('06 AddNewProduct/android.widget.ImageView (1)'), 0)

Mobile.tap(findTestObject('06 AddNewProduct/android.widget.Button (1)'), 0)

Mobile.tap(findTestObject('06 AddNewProduct/android.widget.ImageView (2)'), 0)

WebUI.delay(3)

Mobile.tap(findTestObject('06 AddNewProduct/android.widget.ImageButton (2)'), 0)

Mobile.tap(findTestObject('06 AddNewProduct/android.widget.Button - Preview'), 0)

Mobile.pressBack()

Mobile.tap(findTestObject('Object Repository/06 AddNewProduct/android.widget.Button - Terbitkan'), 0)

WebUI.delay(2)


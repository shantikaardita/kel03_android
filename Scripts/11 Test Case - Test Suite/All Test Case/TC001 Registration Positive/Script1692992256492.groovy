import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

Mobile.setText(findTestObject('Registrasi Akun/Daftar_InputNama'), 'Kelompok 3 Platinum', 0)

Mobile.delay(3, FailureHandling.STOP_ON_FAILURE)

//Generate email
Date email = new Date()

String emailDoctor = email.format('yyyyMMddHHmmss')

def email_code = ('kel03_android' + emailDoctor) + '@gmail.com'

//Set GlobalVariable
GlobalVariable.emailVar = email_code

Mobile.setText(findTestObject('Registrasi Akun/Daftar_InputEmail'), GlobalVariable.emailVar, 0)

Mobile.delay(3, FailureHandling.STOP_ON_FAILURE)

Mobile.setEncryptedText(findTestObject('Registrasi Akun/Daftar_InputPassword'), 'aeHFOx8jV/A=', 0)

Mobile.delay(2, FailureHandling.STOP_ON_FAILURE)

Mobile.setText(findTestObject('Registrasi Akun/Daftar_InputNoHP'), '081231231231', 0)

Mobile.delay(2, FailureHandling.STOP_ON_FAILURE)

Mobile.setText(findTestObject('Registrasi Akun/Daftar_InputKota'), 'Jakarta', 0)

Mobile.scrollToText('', FailureHandling.STOP_ON_FAILURE)

Mobile.setText(findTestObject('Registrasi Akun/Daftar_InputAlamat'), 'Jalan Mawar', 0)

Mobile.delay(2, FailureHandling.STOP_ON_FAILURE)

Mobile.scrollToText('', FailureHandling.STOP_ON_FAILURE)

Mobile.doubleTap(findTestObject('Registrasi Akun/Registrasi_ButtonXpath'), 0)

Mobile.delay(3, FailureHandling.STOP_ON_FAILURE)


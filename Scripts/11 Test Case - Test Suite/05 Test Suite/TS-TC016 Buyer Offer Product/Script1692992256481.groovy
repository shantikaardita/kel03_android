import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

//Mobile.startApplication('C:\\Users\\app-release-second-hand-gcp.apk', false)

Mobile.tap(findTestObject('05 Offer Processing/1501 Registrasi Akun/Icon_Akun'), 0)

Mobile.tap(findTestObject('05 Offer Processing/1501 Registrasi Akun/Button_Masuk'), 0)

Mobile.tap(findTestObject('05 Offer Processing/1501 Registrasi Akun/Button_Daftar'), 0)

Mobile.setText(findTestObject('05 Offer Processing/1501 Registrasi Akun/Daftar_InputNama'), 'Buyer - Kelompok 3 Platinum', 
    0)

//Generate email
Date email = new Date()

String emailDoctor = email.format('yyyyMMddHHmmss')

def email_code = ('buyer_kel03_and' + emailDoctor) + '@gmail.com'

//Set GlobalVariable
GlobalVariable.emailVarBuy = email_code

//Set GlobalVariable for datetime
GlobalVariable.datetimeVar = emailDoctor

Mobile.setText(findTestObject('05 Offer Processing/1501 Registrasi Akun/Daftar_InputEmail'), GlobalVariable.emailVarBuy, 
    0)

Mobile.setEncryptedText(findTestObject('05 Offer Processing/1501 Registrasi Akun/Daftar_InputPassword'), 'aeHFOx8jV/A=', 
    0)

Mobile.setText(findTestObject('05 Offer Processing/1501 Registrasi Akun/Daftar_InputNoHP'), '081231231231', 0)

Mobile.setText(findTestObject('05 Offer Processing/1501 Registrasi Akun/Daftar_InputKota'), 'Jakarta', 0)

Mobile.scrollToText('', FailureHandling.STOP_ON_FAILURE)

Mobile.setText(findTestObject('05 Offer Processing/1501 Registrasi Akun/Daftar_InputAlamat'), 'Jalan Mawar ' + GlobalVariable.datetimeVar, 
    0)

Mobile.scrollToText('', FailureHandling.STOP_ON_FAILURE)

Mobile.tap(findTestObject('05 Offer Processing/1501 Registrasi Akun/Registrasi_ButtonXpath'), 0)

Mobile.waitForElementPresent(findTestObject('05 Offer Processing/1602 Offer Product/01-02 TextView - Beranda'), 10 // adjust the timeout value as needed
    )

Mobile.tap(findTestObject('05 Offer Processing/1602 Offer Product/01-02 TextView - Beranda'), 0, FailureHandling.STOP_ON_FAILURE)

Mobile.waitForElementPresent(findTestObject('05 Offer Processing/1602 Offer Product/02-02Tap EditText - Cari di Second Chance'), 
    10, FailureHandling.STOP_ON_FAILURE)

// Step 1: Tap Search button
Mobile.tap(findTestObject('05 Offer Processing/1602 Offer Product/02-02Tap EditText - Cari di Second Chance'), 0, FailureHandling.STOP_ON_FAILURE)

Mobile.delay(2, FailureHandling.STOP_ON_FAILURE)

// Step 2: Type the search 'MtFuji' GlobalVariable.productIDVar
Mobile.setText(findTestObject('05 Offer Processing/1602 Offer Product/02-03Type EditText - Cari di Second Chance'), GlobalVariable.productIDVar, 
    0)

// Step 3: Wait for the results to show up
Mobile.waitForElementPresent(findTestObject('05 Offer Processing/1602 Offer Product/03 ImageView'), 10 // adjust the timeout value as needed
    )

Mobile.tap(findTestObject('05 Offer Processing/1602 Offer Product/03 ImageView'), 0)

Mobile.delay(2, FailureHandling.STOP_ON_FAILURE)

Mobile.waitForElementPresent(findTestObject('05 Offer Processing/1602 Offer Product/04 Button - Saya Tertarik dan Ingin Nego'), 
    10)

Mobile.tap(findTestObject('05 Offer Processing/1602 Offer Product/04 Button - Saya Tertarik dan Ingin Nego'), 0)

Mobile.waitForElementPresent(findTestObject('05 Offer Processing/1602 Offer Product/05 EditText - Rp 0,00 Harga Tawar'), 
    10, FailureHandling.STOP_ON_FAILURE)

Mobile.setText(findTestObject('05 Offer Processing/1602 Offer Product/05 EditText - Rp 0,00 Harga Tawar'), '99000', 0)

Mobile.delay(2, FailureHandling.STOP_ON_FAILURE)

Mobile.tap(findTestObject('05 Offer Processing/1602 Offer Product/06 Button - Kirim'), 0)

Mobile.delay(3, FailureHandling.STOP_ON_FAILURE)

Mobile.waitForElementPresent(findTestObject('05 Offer Processing/1602 Offer Product/07 ImageView - back'), 10)

Mobile.tap(findTestObject('05 Offer Processing/1602 Offer Product/07 ImageView - back'), 0)

Mobile.waitForElementPresent(findTestObject('05 Offer Processing/1602 Offer Product/08 ImageView - back 02'), 10)

Mobile.tap(findTestObject('05 Offer Processing/1602 Offer Product/08 ImageView - back 02'), 0)

Mobile.waitForElementPresent(findTestObject('05 Offer Processing/1602 Offer Product/09 TextView - Akun'), 10)

Mobile.tap(findTestObject('05 Offer Processing/1602 Offer Product/09 TextView - Akun'), 0)

Mobile.waitForElementPresent(findTestObject('05 Offer Processing/1602 Offer Product/10 TextView - Keluar'), 10)

Mobile.tap(findTestObject('05 Offer Processing/1602 Offer Product/10 TextView - Keluar'), 0)

Mobile.delay(3, FailureHandling.STOP_ON_FAILURE)


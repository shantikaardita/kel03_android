import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

Mobile.startApplication('/Users/telkom/Downloads/app-release-second-hand-gcp.apk', true)

Mobile.tap(findTestObject('Object Repository/04 Profile/01/android.widget.ImageView'), 0)

Mobile.tap(findTestObject('Object Repository/04 Profile/01/android.widget.Button - Masuk'), 0)

Mobile.tap(findTestObject('Object Repository/04 Profile/01/android.widget.EditText - Masukkan email'), 0)

Mobile.setText(findTestObject('Object Repository/04 Profile/01/android.widget.EditText - Masukkan email (1)'), 'widi123@gmail.com', 
    0)

Mobile.tap(findTestObject('Object Repository/04 Profile/01/android.widget.EditText - Masukkan password'), 0)

Mobile.setEncryptedText(findTestObject('Object Repository/04 Profile/01/android.widget.EditText - Masukkan password (1)'), 
    'keN3JctwEfftra09Ynu56w==', 0)

Mobile.tap(findTestObject('Object Repository/04 Profile/01/android.widget.Button - Masuk (1)'), 0)

Mobile.tap(findTestObject('04 Profile/01/android.widget.ImageView (1)'), 0)

Mobile.tap(findTestObject('04 Profile/01/android.widget.ImageView (2)'), 0)

Mobile.tap(findTestObject('Object Repository/04 Profile/01/android.widget.EditText - username'), 0)

Mobile.tap(findTestObject('Object Repository/04 Profile/01/android.widget.ImageButton'), 0)

Mobile.setText(findTestObject('04 Profile/01/android.widget.EditText - Nama'), 'widi alfrida pramesti', 0)

Mobile.hideKeyboard()

Mobile.tap(findTestObject('Object Repository/04 Profile/01/android.widget.Button - Simpan'), 0)

Mobile.tap(findTestObject('Object Repository/04 Profile/01/android.widget.ImageView (5)'), 0)

Mobile.tap(findTestObject('Object Repository/04 Profile/01/android.widget.EditText - address'), 0)

Mobile.tap(findTestObject('Object Repository/04 Profile/01/android.widget.ImageButton (1)'), 0)

Mobile.setText(findTestObject('Object Repository/04 Profile/01/android.widget.EditText - Alamat'), 'jalan haji nawi', 0)

Mobile.hideKeyboard()

Mobile.tap(findTestObject('04 Profile/01/android.widget.Button - Simpan'), 0)

Mobile.closeApplication()


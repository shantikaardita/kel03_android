import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

Mobile.startApplication('C:\\Users\\app-release-second-hand-gcp.apk', false)

Mobile.tap(findTestObject('TC003_Login/android.widget.TextView - Akun'), 0)

Mobile.tap(findTestObject('TC003_Login/android.widget.Button - Masuk_01'), 0)

Mobile.setText(findTestObject('TC003_Login/android.widget.EditText - Masukkan email'), 'kel03_and@gmail.com', 0)

Mobile.setEncryptedText(findTestObject('TC003_Login/android.widget.EditText - Masukkan password'), 'aeHFOx8jV/A=', 0)

Mobile.tap(findTestObject('TC003_Login/android.widget.Button - Masuk_02'), 0)

Mobile.waitForElementPresent(findTestObject('TC003_Login/android.widget.TextView - Akun Saya'), 10)

Mobile.verifyElementVisible(findTestObject('TC003_Login/android.widget.TextView - Akun Saya'), 0)


package com.seconhandmobile
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class SellerDeclineOfferSteps {
  
  @Given("the seller starts the mobile application")
  void the_seller_starts_the_mobile_application() {
    Mobile.startApplication('C:\\Users\\app-release-second-hand-gcp.apk', false)
  }

  @And("the seller logs into the application")
  void the_seller_logs_into_the_application() {
    // Logging in steps here
    // ...
	  Mobile.tap(findTestObject('TC003_Login/android.widget.TextView - Akun'), 0)
	  
	  Mobile.tap(findTestObject('TC003_Login/android.widget.Button - Masuk_01'), 0)
	  
	  Mobile.setText(findTestObject('TC003_Login/android.widget.EditText - Masukkan email'), 'kel03fnbuyer@gmail.com', 0)
	  
	  Mobile.setEncryptedText(findTestObject('TC003_Login/android.widget.EditText - Masukkan password'), 'aeHFOx8jV/A=', 0)
	  
	  Mobile.tap(findTestObject('TC003_Login/android.widget.Button - Masuk_02'), 0)
  }

  @When("the seller navigates to the \"Daftar Jual Saya\" screen")
  void the_seller_navigates_to_the_daftar_jual_saya_screen() {
    // Navigation steps here
    // ...
	  Mobile.tap(findTestObject('05 Offer Processing/1702 Respond Offer/01 TextView - Daftar Jual Saya'), 0)
  }

  @And("the seller selects the \"Diminati\" tab")
  void the_seller_selects_the_diminati_tab() {
    // Selection steps here
    // ...
	  Mobile.tap(findTestObject('05 Offer Processing/1702 Respond Offer/02 TextView - Diminati'), 0)
  }

  @Then("the seller should see available offers")
  void the_seller_should_see_available_offers() {
    // Validation steps here
    // ...
	  Mobile.waitForElementPresent(findTestObject('05 Offer Processing/1702 Respond Offer/03 TextView - Penawaran produk'), 10)
	  
	  Mobile.tap(findTestObject('05 Offer Processing/1702 Respond Offer/03 TextView - Penawaran produk'), 0)
  }

  @When("the seller chooses to decline an offer")
  void the_seller_chooses_to_decline_an_offer() {
    // Decline offer steps here
    // ...
	  Mobile.tap(findTestObject('05 Offer Processing/1702 Respond Offer/04-02 Button - Tolak'), 0)
  }

  @Then("the offer should be successfully declined")
  void the_offer_should_be_successfully_declined() {
    // Validation steps here
    // ...
	  Mobile.waitForElementPresent(findTestObject('05 Offer Processing/1702 Respond Offer/05-02 TextView - Penawaran ditolak'),
		  10)
	  
	  Mobile.verifyElementVisible(findTestObject('05 Offer Processing/1702 Respond Offer/05-02 TextView - Penawaran ditolak'),
		  0)
	  
	  Mobile.delay(5, FailureHandling.STOP_ON_FAILURE)
  }

  @And("the seller logs out")
  void the_seller_logs_out() {
    // Logout steps here
    // ...
	  Mobile.waitForElementPresent(findTestObject('05 Offer Processing/1702 Respond Offer/06-02 ImageView - back'), 10)
	  
	  Mobile.tap(findTestObject('05 Offer Processing/1702 Respond Offer/06-02 ImageView - back'), 0)
	  
	  Mobile.waitForElementPresent(findTestObject('05 Offer Processing/1702 Respond Offer/07-02 ImageView - back 02'), 10)
	  
	  Mobile.tap(findTestObject('05 Offer Processing/1702 Respond Offer/07-02 ImageView - back 02'), 0)
	  
	  Mobile.waitForElementPresent(findTestObject('05 Offer Processing/1702 Respond Offer/08-02 TextView - Keluar'), 10)
	  
	  Mobile.checkElement(findTestObject('05 Offer Processing/1702 Respond Offer/08-02 TextView - Keluar'), 0)
	  
	  Mobile.delay(3, FailureHandling.STOP_ON_FAILURE)
	  
  }
}
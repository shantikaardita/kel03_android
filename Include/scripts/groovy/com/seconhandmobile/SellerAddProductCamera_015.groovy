package com.seconhandmobile
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When

import internal.GlobalVariable as GlobalVariable

class SellerAddProductCamera_015 {

	@Given("I cam open the SecondHand mobile application")
	def iOpenTheSecondHandMobileApplication() {
		Mobile.startApplication('C:\\Users\\app-release-second-hand-gcp.apk', false)
	}

	@Given("I cam navigate to the account tab")
	def iNavigateToTheAccountTab() {
		Mobile.waitForElementPresent(findTestObject('05 Offer Processing/1501 Registrasi Akun/Icon_Akun'), 10)
		Mobile.tap(findTestObject('05 Offer Processing/1501 Registrasi Akun/Icon_Akun'), 0)
	}

	@Given("I cam log in as a new seller")
	def iLogInAsANewSeller() {
		// Code to register and log in as a new seller
		Mobile.tap(findTestObject('05 Offer Processing/1501 Registrasi Akun/Button_Masuk'), 0)

		Mobile.tap(findTestObject('05 Offer Processing/1501 Registrasi Akun/Button_Daftar'), 0)

		Mobile.setText(findTestObject('05 Offer Processing/1501 Registrasi Akun/Daftar_InputNama'), 'Seller - Kelompok 3 Platinum',
				0)

		//Generate email
		Date email = new Date()

		String emailDoctor = email.format('yyyyMMddHHmmss')

		def email_code = ('seller_kel03_and' + emailDoctor) + '@gmail.com'

		//Set GlobalVariable
		GlobalVariable.emailVarSel = email_code

		//Set GlobalVariable for datetime
		GlobalVariable.datetimeVar = emailDoctor

		Mobile.setText(findTestObject('05 Offer Processing/1501 Registrasi Akun/Daftar_InputEmail'), GlobalVariable.emailVarSel,
				0)

		Mobile.setEncryptedText(findTestObject('05 Offer Processing/1501 Registrasi Akun/Daftar_InputPassword'), 'aeHFOx8jV/A=',
				0)

		Mobile.setText(findTestObject('05 Offer Processing/1501 Registrasi Akun/Daftar_InputNoHP'), '081231231231', 0)

		Mobile.setText(findTestObject('05 Offer Processing/1501 Registrasi Akun/Daftar_InputKota'), 'Jakarta', 0)

		Mobile.scrollToText('', FailureHandling.STOP_ON_FAILURE)

		Mobile.setText(findTestObject('05 Offer Processing/1501 Registrasi Akun/Daftar_InputAlamat'), 'Jalan Mawar ' + GlobalVariable.datetimeVar,
				0)

		Mobile.scrollToText('', FailureHandling.STOP_ON_FAILURE)

		Mobile.tap(findTestObject('05 Offer Processing/1501 Registrasi Akun/Registrasi_ButtonXpath'), 0)
	}

	@When("I cam navigate to the add product page")
	def iNavigateToAddProductPage() {
		Mobile.waitForElementPresent(findTestObject('05 Offer Processing/1502 Add Product/Button - Plus'), 10)
		Mobile.tap(findTestObject('05 Offer Processing/1502 Add Product/Button - Plus'), 0)
	}

	@When("I cam fill in the product details")
	def iFillInTheProductDetails() {
		// Code to fill in the product details
		def product_code = 'MtFuji' + GlobalVariable.datetimeVar

		//Set GlobalVariable productIDname
		GlobalVariable.productIDVar = product_code

		Mobile.setText(findTestObject('05 Offer Processing/1502 Add Product/Text - Nama Produk'), GlobalVariable.productIDVar, 0)

		Mobile.setText(findTestObject('05 Offer Processing/1502 Add Product/Text - Harga Produk'), '100000', 0)

		Mobile.tap(findTestObject('05 Offer Processing/1502 Add Product/Spinner - Pilih Kategori'), 0)

		Mobile.delay(2, FailureHandling.STOP_ON_FAILURE)

		Mobile.tap(findTestObject('05 Offer Processing/1502 Add Product/Text - Lokasi Produk'), 0, FailureHandling.STOP_ON_FAILURE)

		Mobile.setText(findTestObject('05 Offer Processing/1502 Add Product/Text - Lokasi Produk'), 'Jakarta', 0)

		Mobile.setText(findTestObject('05 Offer Processing/1502 Add Product/Text - Deskripsi'), 'Mount Fuji Summer 2023', 0)

		Mobile.delay(2, FailureHandling.STOP_ON_FAILURE)
	}

	@When("I cam upload a product image")
	def iUploadAProductImage() {
		// Code to upload a product image
		Mobile.tap(findTestObject('05 Offer Processing/1502 Add Product/Cam01 - btn - Foto Product'), 0)
		
		Mobile.waitForElementPresent(findTestObject('05 Offer Processing/1502 Add Product/Cam02 - Button - Kamera'), 10)
		
		Mobile.tap(findTestObject('05 Offer Processing/1502 Add Product/Cam02 - Button - Kamera'), 0)
		
		Mobile.waitForElementPresent(findTestObject('05 Offer Processing/1502 Add Product/Cam03 - ImageView - Shoot'), 10)
		
		Mobile.tap(findTestObject('05 Offer Processing/1502 Add Product/Cam03 - ImageView - Shoot'), 0)
		
		Mobile.waitForElementPresent(findTestObject('05 Offer Processing/1502 Add Product/Cam04 - ImageButton - Check OK'), 10)
		
		Mobile.tap(findTestObject('05 Offer Processing/1502 Add Product/Cam04 - ImageButton - Check OK'), 0)
	}

	@When("I cam publish the product")
	def iPublishTheProduct() {
		Mobile.waitForElementPresent(findTestObject('05 Offer Processing/1502 Add Product/Button - Terbitkan'), 10)
		Mobile.tap(findTestObject('05 Offer Processing/1502 Add Product/Button - Terbitkan'), 0)
	}

	@Then("the product cam should be successfully added")
	def theProductShouldBeSuccessfullyAdded() {
		Mobile.waitForElementPresent(findTestObject('05 Offer Processing/1502 Add Product/99-01 ViewGroup'), 10)
		Mobile.verifyElementVisible(findTestObject('05 Offer Processing/1502 Add Product/99-02 ImageView - trash'), 0)
		Mobile.delay(2, FailureHandling.STOP_ON_FAILURE)

		Mobile.tap(findTestObject('05 Offer Processing/1502 Add Product/99-03 ImageView - back'), 0)

		Mobile.tap(findTestObject('05 Offer Processing/1502 Add Product/99-04 TextView - Keluar'), 0)

		Mobile.delay(3, FailureHandling.STOP_ON_FAILURE)
	}
}
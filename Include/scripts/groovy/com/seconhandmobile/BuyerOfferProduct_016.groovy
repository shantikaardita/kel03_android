package com.seconhandmobile
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class BuyerOfferProductSteps {
  
  @Given("the user starts the mobile application")
  void the_user_starts_the_mobile_application() {
    Mobile.startApplication('C:\\Users\\app-release-second-hand-gcp.apk', false)
  }

  @And("the user navigates to the registration screen")
  void the_user_navigates_to_the_registration_screen() {
    Mobile.tap(findTestObject('05 Offer Processing/1501 Registrasi Akun/Icon_Akun'), 0)
    Mobile.tap(findTestObject('05 Offer Processing/1501 Registrasi Akun/Button_Masuk'), 0)
    Mobile.tap(findTestObject('05 Offer Processing/1501 Registrasi Akun/Button_Daftar'), 0)
  }

  @When("the user registers a new account")
  void the_user_registers_a_new_account() {
    // Registration steps here
    // ...
	  Mobile.setText(findTestObject('05 Offer Processing/1501 Registrasi Akun/Daftar_InputNama'), 'Buyer - Kelompok 3 Platinum',
		  0)
	  
	  //Generate email
	  Date email = new Date()
	  
	  String emailDoctor = email.format('yyyyMMddHHmmss')
	  
	  def email_code = ('buyer_kel03_and' + emailDoctor) + '@gmail.com'
	  
	  //Set GlobalVariable
	  GlobalVariable.emailVarBuy = email_code
	  
	  //Set GlobalVariable for datetime
	  GlobalVariable.datetimeVar = emailDoctor
	  
	  Mobile.setText(findTestObject('05 Offer Processing/1501 Registrasi Akun/Daftar_InputEmail'), GlobalVariable.emailVarBuy,
		  0)
	  
	  Mobile.setEncryptedText(findTestObject('05 Offer Processing/1501 Registrasi Akun/Daftar_InputPassword'), 'aeHFOx8jV/A=',
		  0)
	  
	  Mobile.setText(findTestObject('05 Offer Processing/1501 Registrasi Akun/Daftar_InputNoHP'), '081231231231', 0)
	  
	  Mobile.setText(findTestObject('05 Offer Processing/1501 Registrasi Akun/Daftar_InputKota'), 'Jakarta', 0)
	  
	  Mobile.scrollToText('', FailureHandling.STOP_ON_FAILURE)
	  
	  Mobile.setText(findTestObject('05 Offer Processing/1501 Registrasi Akun/Daftar_InputAlamat'), 'Jalan Mawar ' + GlobalVariable.datetimeVar,
		  0)
	  
	  Mobile.scrollToText('', FailureHandling.STOP_ON_FAILURE)
	  
	  Mobile.tap(findTestObject('05 Offer Processing/1501 Registrasi Akun/Registrasi_ButtonXpath'), 0)
	
  }

  @And("the user searches for a product named MtFuji")
  void the_user_searches_for_a_product_named() {
    // Search steps here
	  Mobile.waitForElementPresent(findTestObject('05 Offer Processing/1602 Offer Product/01-02 TextView - Beranda'), 10 // adjust the timeout value as needed
	  )
  
	  Mobile.tap(findTestObject('05 Offer Processing/1602 Offer Product/01-02 TextView - Beranda'), 0, FailureHandling.STOP_ON_FAILURE)
  
	  Mobile.waitForElementPresent(findTestObject('05 Offer Processing/1602 Offer Product/02-02Tap EditText - Cari di Second Chance'),
	  10, FailureHandling.STOP_ON_FAILURE)
  
	  // Step 1: Tap Search button
	  Mobile.tap(findTestObject('05 Offer Processing/1602 Offer Product/02-02Tap EditText - Cari di Second Chance'), 0, FailureHandling.STOP_ON_FAILURE)
  
	  Mobile.delay(2, FailureHandling.STOP_ON_FAILURE)
  
	  // Step 2: Type the search 'MtFuji'
	  Mobile.setText(findTestObject('05 Offer Processing/1602 Offer Product/02-03Type EditText - Cari di Second Chance'), 'MtFuji',
	  0)
  }

  @Then("the user should see product results")
  void the_user_should_see_product_results() {
    // Validation steps here
    // ...
	  // Step 3: Wait for the results to show up
	  Mobile.waitForElementPresent(findTestObject('05 Offer Processing/1602 Offer Product/03 ImageView'), 10 // adjust the timeout value as needed
		  )
	  
	  Mobile.tap(findTestObject('05 Offer Processing/1602 Offer Product/03 ImageView'), 0)
	  
	  Mobile.delay(2, FailureHandling.STOP_ON_FAILURE)
  }

  @And("the user makes an offer for the product")
  void the_user_makes_an_offer_for_the_product() {
    // Offer steps here
    // ...
	  Mobile.waitForElementPresent(findTestObject('05 Offer Processing/1602 Offer Product/04 Button - Saya Tertarik dan Ingin Nego'),
		  10)
	  
	  Mobile.tap(findTestObject('05 Offer Processing/1602 Offer Product/04 Button - Saya Tertarik dan Ingin Nego'), 0)
	  
	  Mobile.waitForElementPresent(findTestObject('05 Offer Processing/1602 Offer Product/05 EditText - Rp 0,00 Harga Tawar'),
		  10, FailureHandling.STOP_ON_FAILURE)
	  
	  Mobile.setText(findTestObject('05 Offer Processing/1602 Offer Product/05 EditText - Rp 0,00 Harga Tawar'), '99000', 0)
	  
	  Mobile.delay(2, FailureHandling.STOP_ON_FAILURE)
	  
	  Mobile.tap(findTestObject('05 Offer Processing/1602 Offer Product/06 Button - Kirim'), 0)
	  
	  Mobile.delay(3, FailureHandling.STOP_ON_FAILURE)
  }

  @Then("the user should see the offer is successful")
  void the_user_should_see_the_offer_is_successful() {
    // Validation steps here
    // ...
	  Mobile.waitForElementPresent(findTestObject('05 Offer Processing/1602 Offer Product/07 ImageView - back'), 10)
	  
	  Mobile.tap(findTestObject('05 Offer Processing/1602 Offer Product/07 ImageView - back'), 0)
	  
	  Mobile.waitForElementPresent(findTestObject('05 Offer Processing/1602 Offer Product/08 ImageView - back 02'), 10)
	  
	  Mobile.tap(findTestObject('05 Offer Processing/1602 Offer Product/08 ImageView - back 02'), 0)
	  
  }

  @And("the user logs out")
  void the_user_logs_out() {
    // Logout steps here
    // ...
	  Mobile.waitForElementPresent(findTestObject('05 Offer Processing/1602 Offer Product/09 TextView - Akun'), 10)
	  
	  Mobile.tap(findTestObject('05 Offer Processing/1602 Offer Product/09 TextView - Akun'), 0)
	  
	  Mobile.waitForElementPresent(findTestObject('05 Offer Processing/1602 Offer Product/10 TextView - Keluar'), 10)
	  
	  Mobile.tap(findTestObject('05 Offer Processing/1602 Offer Product/10 TextView - Keluar'), 0)
	  
	  Mobile.delay(3, FailureHandling.STOP_ON_FAILURE)
  }
}
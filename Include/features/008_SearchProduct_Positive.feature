#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@tag
Feature: User Can Search Product
  I want to use this template for my feature file

  @tag1
  Scenario Outline: Title of your scenario outline
    Given open an application, click user profile, then input <email> and <password> for login
    When User click the home button
    And User click the search bar
    And User input Produk Wididay Mobile
    Then list of Product Wididay Mobile shown
    When user click one Product Wididay from the list
    Then the detail of Product Wididay Mobile shown

    Examples: 
      | email  | password | 
      | email  | pass     | 
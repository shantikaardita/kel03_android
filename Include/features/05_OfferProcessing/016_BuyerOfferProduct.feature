#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@tag
Feature: Buyer Offer Product

  Scenario: Buyer offers a product successfully
    Given the user starts the mobile application
    And the user navigates to the registration screen
    When the user registers a new account
    And the user searches for a product named MtFuji
    Then the user should see product results
    And the user makes an offer for the product
    Then the user should see the offer is successful
    And the user logs out
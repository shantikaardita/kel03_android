#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@tag
Feature: Add a New Product as a Seller

  Scenario: Seller successfully adds a new product
    Given I cam open the SecondHand mobile application
    And I cam navigate to the account tab
    And I cam log in as a new seller
    When I cam navigate to the add product page
    And I cam fill in the product details
    And I cam upload a product image
    And I cam publish the product
    Then the product cam should be successfully added
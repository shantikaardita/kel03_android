#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@tag
Feature: Seller Decline Offer

  Scenario: Seller declines an offer successfully
    Given the seller starts the mobile application
    And the seller logs into the application
    When the seller navigates to the "Daftar Jual Saya" screen
    And the seller selects the "Diminati" tab
    Then the seller should see available offers
    When the seller chooses to decline an offer
    Then the offer should be successfully declined
    And the seller logs out
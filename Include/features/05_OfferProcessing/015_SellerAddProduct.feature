#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@tag
Feature: Add a New Product as a Seller

  Scenario: Seller successfully adds a new product
    Given I open the SecondHand mobile application
    And I navigate to the account tab
    And I log in as a new seller
    When I navigate to the add product page
    And I fill in the product details
    And I upload a product image
    And I publish the product
    Then the product should be successfully added
#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@tag
Feature: Positive Login for SecondHand Application

  Scenario: Successful login with valid credentials
    Given II FN open the SecondHand mobile application
    When II FN navigate to the account tab
    And II FN tap on the Login button
    And II FN enter the valid email as "kel03_and@gmail.com"
    And II FN enter the valid password
    And II FN submit the login form
    Then II FN should not be able to login